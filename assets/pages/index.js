SWXApp.namespace('pages.index');

SWXApp.pages.index = (function() {
    //Dependency
    var List = SWXApp.modules.List;
    //module attribute
    var pageData =  new List([1, 2, 3, 4]);

    var init = function() {
        $('body').append('<h1>Wellcome to index page.</h1>');
        console.log(pageData.toArray());
        pageData.add(5);
        console.log(pageData.toArray());
        pageData.remove(0);
        console.log(pageData.toArray());
    };

    return {
    	init: init
    };
})();
