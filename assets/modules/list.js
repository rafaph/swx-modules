SWXApp.namespace('modules.List');

SWXApp.modules.List = (function() {
    'use strict';

    var isArray = function(o) {
        return '[object Array]' === Object.prototype.toString.call(o);
    };

    var InnerClass = function(o) {
        if(isArray(o)) {
            this.elements = o.slice();
        } else {
            throw Error('Param must be a array');
        }
    };

    InnerClass.prototype = {
        constructor: SWXApp.modules.List,
        version: '1.0',
        isArray: isArray,
        get: function(index) {
            if(index >= 0 && index < this.elements.length) {
                return this.elements[index];
            }
            return null;
        },
        add: function(element, position) {
            if(typeof position === 'undefined') {
                this.elements.push(element);
            } else {
                if(position >= 0) {
                    this.elements.splice(position, 0, element);
                } else {
                    throw Error('Position must be greater or equal than zero.');
                }
            }
        },
        remove: function(index) {
            if(index >= 0 && index < this.elements.length) {
                return this.elements.splice(index, 1)[0];
            }
            return null;
        },
        set: function(element, position) {
            if(position >= 0 && position < this.elements.length) {
                this.elements[position] = element;
            } else {
                throw Error('Position must be greater than zero and less than ' + this.elements.length);
            }
        },
        pop: function() {
            if(this.elements.length > 0) {
                return this.elements.pop();
            }
            return null;
        },
        shift: function() {
            if(this.elements.length > 0) {
                return this.elements.shift();
            }
            return null;
        },
        contains: function(obj) {
            return this.elements.indexOf(obj) > -1;
        },
        toArray: function() {
            return this.elements.slice();
        }
    };

    return InnerClass;
})();
