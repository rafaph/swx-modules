var SWXApp = {};
/**
 * Essa funcao garante que mesmo que os arquivos sejam concatenados na ordem errada e
 * um dependa do outro, o codigo ainda funcione.
 * 
 * @param  {String} strModules Modulos separados por ponto
 * @return {Object} Modulo existente
 */
SWXApp.namespace = function(strModules) {
    var parts = strModules.split('.');
    var parent = SWXApp;
    var numParts;
    var i;

    if(parts[0] === 'SWXApp') {
        parts = parts.slice(1);
    }
    numParts = parts.length;

    for(var i = 0; i < numParts; i++) {
        if(typeof parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    };

    return parent;
};

SWXApp.init = function() {
    //shortcut for $(document).ready(fn);
    $(function() {
        'use strict';
        var page = $('[data-page]').data().page;

        if(SWXApp.pages.hasOwnProperty(page)) {
            SWXApp.pages[page].init();
        } else {
            throw Error(page + ' not found.');
        }

    });
};

SWXApp.init();
