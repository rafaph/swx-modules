'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var del = require('del');
var webserver = require('gulp-webserver');
var gutil = require('gulp-util');
var gconcat = require('gulp-concat');

var DEST = 'app/static/';

gulp.task('clean:js', function() {
    return del([
        DEST + '*'
    ]);
});

gulp.task('build:js', ['clean:js'], function() {
    var b = gulp.src([
            'bower_components/jquery/dist/jquery.js',
            'assets/app.js',
            'assets/modules/*.js',
            'assets/pages/*.js'
        ])
        .pipe(gconcat('app.js'))
        .pipe(gulp.dest(DEST));
    if(gutil.env.prod) {
        b.pipe(uglify())
            .pipe(gulp.dest(DEST));
    }
    return b;
});

gulp.task('default', ['build:js'], function() {
    gulp.src('app')
        .pipe(webserver({
            port: 3000
        }))
});
